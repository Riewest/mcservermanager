﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MCServerManager;

namespace MCServerManager.Pages
{
    public class SettingsModel : PageModel
    {
        public SettingsModel()
        {
        }

        [BindProperty]
        public Settings Settings { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {

            Settings = Settings.LoadSettings();

            if (Settings == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            try
            {
                Settings.SaveSettings();
            } catch (Exception e)
            {

            }

            return RedirectToPage("./Index");
        }
    }
}
