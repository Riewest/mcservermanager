﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace MCServerManager.Pages
{
    public class IndexModel : PageModel
    {
        private readonly MCServerContext _db;

        public IList<MCServer> MCServer { get; set; }
        public IList<int> ActiveServerIds { get; set; }

        public IndexModel(MCServerContext db)
        {
            _db = db;
        }

        public void OnGet()
        {
            MCServer = _db.GetAllMCServers();
            ActiveServerIds = MCServerManagerClass.Instance.GetActiveServers().Select(x => x.Id).ToList();
        }

        public IActionResult OnPostDelete(int id)
        {
            var server = _db.FindMCServer(id);
            if (!string.IsNullOrEmpty(server.Name))
            {
                _db.RemoveMCServer(id);
            }
            return RedirectToPage();
        }
    }
}
