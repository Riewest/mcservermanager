﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MCServerManager;
using MCServerManager.Hubs;
using Microsoft.AspNetCore.SignalR;
using System.Threading;
using System.IO;

namespace MCServerManager.Pages
{
    public class ServerDetailModel : PageModel
    {
        private readonly MCServerContext _db;
        private readonly IHubContext<ServerHub> _serverHub;

        public ServerDetailModel(MCServerContext db, IHubContext<ServerHub> serverHub)
        {
            _db = db;
            _serverHub = serverHub;
        }
        public string Filename { get; set; }
        public string Folder { get; set; }
        public int ServerId { get; set; }
        public MCServer MCServer { get; set; }

        public bool IsActive
        {
            get
            {
                if(MCServer == null)
                {
                    return false;
                }
                return MCServerManagerClass.Instance.GetActiveById(MCServer.Id);
            }
        }

        public IActionResult OnGet(int? id)
        {
            if (!id.HasValue)
            {
                return NotFound();
            }


            MCServer = _db.FindMCServer((int)id);

            if (MCServer.Name == null)
            {
                return NotFound();
            }
            return Page();
        }

        public void OnPostStart(int id)
        {
            var server = _db.FindMCServer(id);
            server._serverHub = this._serverHub;
            if (!string.IsNullOrEmpty(server.Name) && !MCServerManagerClass.Instance.GetActiveById(id))
            {
                MCServerManagerClass.Instance.AddServer(server);
                MCServerManagerClass.Instance.StartServerById(id);
            }
            MCServer = server;
        }

    }
}
