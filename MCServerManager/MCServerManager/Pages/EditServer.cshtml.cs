﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MCServerManager;


namespace MCServerManager.Pages
{
    public class EditServerModel : PageModel
    {
        private readonly MCServerContext _db;

        public EditServerModel(MCServerContext db)
        {
            _db = db;
        }

        [BindProperty]
        public MCServer MCServer { get; set; }

        public IActionResult OnGet(int? id)
        {
            if (!id.HasValue)
            {
                return NotFound();
            }

            MCServer = _db.FindMCServer((int)id);

            if (MCServer == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }           

            try
            {
                _db.UpdateMCServer(MCServer);
                MCServerManagerClass.Instance.UpdateServer(MCServer);
            }
            catch (Exception e)
            {
                throw e;
            }

            return RedirectToPage("./Index");
        }
    }
}
