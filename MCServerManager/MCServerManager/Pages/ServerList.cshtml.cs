﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MCServerManager;


namespace MCServerManager.Pages
{
    public class ServerListModel : PageModel
    {
        private readonly MCServerContext _db;

        public ServerListModel(MCServerContext db)
        {
            _db = db;
        }

        public IList<MCServer> MCServer { get;set; }

        public void OnGet()
        {
            MCServer = _db.GetAllMCServers();
        }
    }
}
