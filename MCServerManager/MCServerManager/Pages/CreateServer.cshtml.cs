﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using MCServerManager;
using System.IO;

namespace MCServerManager.Pages
{
    public class CreateServerModel : PageModel
    {
        private readonly MCServerContext _db;

        public CreateServerModel(MCServerContext db)
        {
            _db = db;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public MCServer MCServer { get; set; }

        public IActionResult OnPost()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _db.AddMCServer(MCServer);

            if(MCServer.CreateDirectory && !Directory.Exists(MCServer.Folder))
            {
                MCServer.MakeDirectory(MCServer.Folder);
            }

            return RedirectToPage("./Index");
        }

    }
}