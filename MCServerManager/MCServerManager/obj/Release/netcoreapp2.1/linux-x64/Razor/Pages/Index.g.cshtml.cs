#pragma checksum "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "dcaf29dc6b7951de120702de6ffaf3bf4a1c3bf7"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(MCServerManager.Pages.Pages_Index), @"mvc.1.0.razor-page", @"/Pages/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.RazorPages.Infrastructure.RazorPageAttribute(@"/Pages/Index.cshtml", typeof(MCServerManager.Pages.Pages_Index), null)]
namespace MCServerManager.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\_ViewImports.cshtml"
using MCServerManager;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"dcaf29dc6b7951de120702de6ffaf3bf4a1c3bf7", @"/Pages/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"21e20d1a8d31c90f78c5a9c10380ac8a7726ef43", @"/Pages/_ViewImports.cshtml")]
    public class Pages_Index : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-page", "./ServerDetail", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-page", "./EditServer", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", new global::Microsoft.AspNetCore.Html.HtmlString("submit"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("style", new global::Microsoft.AspNetCore.Html.HtmlString("float: right;"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-page-handler", "delete", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormActionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormActionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 3 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\Index.cshtml"
  
    ViewData["Title"] = "Minecraft Server Manager";

#line default
#line hidden
            BeginContext(86, 4, true);
            WriteLiteral("\r\n\r\n");
            EndContext();
            BeginContext(90, 2764, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a7cd1af04399445d9f153e84d04e678a", async() => {
                BeginContext(110, 179, true);
                WriteLiteral("\r\n    <table class=\"table\">\r\n        <thead>\r\n            <tr>\r\n                <th>\r\n                    Active\r\n                </th>\r\n                <th>\r\n                    ");
                EndContext();
                BeginContext(290, 52, false);
#line 16 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\Index.cshtml"
               Write(Html.DisplayNameFor(model => model.MCServer[0].Name));

#line default
#line hidden
                EndContext();
                BeginContext(342, 67, true);
                WriteLiteral("\r\n                </th>\r\n                <th>\r\n                    ");
                EndContext();
                BeginContext(410, 55, false);
#line 19 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\Index.cshtml"
               Write(Html.DisplayNameFor(model => model.MCServer[0].Modpack));

#line default
#line hidden
                EndContext();
                BeginContext(465, 67, true);
                WriteLiteral("\r\n                </th>\r\n                <th>\r\n                    ");
                EndContext();
                BeginContext(533, 52, false);
#line 22 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\Index.cshtml"
               Write(Html.DisplayNameFor(model => model.MCServer[0].Port));

#line default
#line hidden
                EndContext();
                BeginContext(585, 67, true);
                WriteLiteral("\r\n                </th>\r\n                <th>\r\n                    ");
                EndContext();
                BeginContext(653, 54, false);
#line 25 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\Index.cshtml"
               Write(Html.DisplayNameFor(model => model.MCServer[0].MaxRam));

#line default
#line hidden
                EndContext();
                BeginContext(707, 67, true);
                WriteLiteral("\r\n                </th>\r\n                <th>\r\n                    ");
                EndContext();
                BeginContext(775, 51, false);
#line 28 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\Index.cshtml"
               Write(Html.DisplayNameFor(model => model.MCServer[0].Jar));

#line default
#line hidden
                EndContext();
                BeginContext(826, 67, true);
                WriteLiteral("\r\n                </th>\r\n                <th>\r\n                    ");
                EndContext();
                BeginContext(894, 54, false);
#line 31 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\Index.cshtml"
               Write(Html.DisplayNameFor(model => model.MCServer[0].Folder));

#line default
#line hidden
                EndContext();
                BeginContext(948, 113, true);
                WriteLiteral("\r\n                </th>\r\n                <th>Actions</th>\r\n            </tr>\r\n        </thead>\r\n        <tbody>\r\n");
                EndContext();
#line 37 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\Index.cshtml"
             foreach (var item in Model.MCServer)
            {

#line default
#line hidden
                BeginContext(1127, 48, true);
                WriteLiteral("                <tr>\r\n                    <td>\r\n");
                EndContext();
#line 41 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\Index.cshtml"
                           
                            string green = "#4cff00";
                            string red = "#ff0000";
                            var active = Model.ActiveServerIds.Contains(item.Id);
                            string color = (active) ? green : red;
                        

#line default
#line hidden
                BeginContext(1490, 29, true);
                WriteLiteral("                        <span");
                EndContext();
                BeginWriteAttribute("style", " style=\"", 1519, "\"", 1622, 11);
                WriteAttributeValue("", 1527, "height:", 1527, 7, true);
                WriteAttributeValue(" ", 1534, "25px;", 1535, 6, true);
                WriteAttributeValue(" ", 1540, "width:", 1541, 7, true);
                WriteAttributeValue(" ", 1547, "25px;", 1548, 6, true);
                WriteAttributeValue(" ", 1553, "background-color:", 1554, 18, true);
#line 47 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\Index.cshtml"
WriteAttributeValue(" ", 1571, color, 1572, 6, false);

#line default
#line hidden
                WriteAttributeValue("", 1578, ";", 1578, 1, true);
                WriteAttributeValue(" ", 1579, "border-radius:", 1580, 15, true);
                WriteAttributeValue(" ", 1594, "50%;", 1595, 5, true);
                WriteAttributeValue(" ", 1599, "display:", 1600, 9, true);
                WriteAttributeValue(" ", 1608, "inline-block;", 1609, 14, true);
                EndWriteAttribute();
                BeginContext(1623, 87, true);
                WriteLiteral("></span>\r\n                    </td>\r\n                    <td>\r\n                        ");
                EndContext();
                BeginContext(1710, 67, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "4b77c4f5ca2d4f17be40b667b69dfa78", async() => {
                    BeginContext(1764, 9, false);
#line 50 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\Index.cshtml"
                                                                        Write(item.Name);

#line default
#line hidden
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Page = (string)__tagHelperAttribute_0.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
                if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
                {
                    throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
                }
                BeginWriteTagHelperAttribute();
#line 50 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\Index.cshtml"
                                                       WriteLiteral(item.Id);

#line default
#line hidden
                __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(1777, 79, true);
                WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
                EndContext();
                BeginContext(1857, 42, false);
#line 53 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Modpack));

#line default
#line hidden
                EndContext();
                BeginContext(1899, 79, true);
                WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
                EndContext();
                BeginContext(1979, 39, false);
#line 56 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Port));

#line default
#line hidden
                EndContext();
                BeginContext(2018, 79, true);
                WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
                EndContext();
                BeginContext(2098, 41, false);
#line 59 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.MaxRam));

#line default
#line hidden
                EndContext();
                BeginContext(2139, 79, true);
                WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
                EndContext();
                BeginContext(2219, 38, false);
#line 62 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Jar));

#line default
#line hidden
                EndContext();
                BeginContext(2257, 79, true);
                WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
                EndContext();
                BeginContext(2337, 41, false);
#line 65 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Folder));

#line default
#line hidden
                EndContext();
                BeginContext(2378, 55, true);
                WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n");
                EndContext();
#line 68 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\Index.cshtml"
                         if (!active)
                        {

#line default
#line hidden
                BeginContext(2499, 28, true);
                WriteLiteral("                            ");
                EndContext();
                BeginContext(2527, 59, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a37dcc843d4044b1b58e2bf0da3ed68a", async() => {
                    BeginContext(2578, 4, true);
                    WriteLiteral("Edit");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Page = (string)__tagHelperAttribute_1.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
                if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
                {
                    throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
                }
                BeginWriteTagHelperAttribute();
#line 70 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\Index.cshtml"
                                                         WriteLiteral(item.Id);

#line default
#line hidden
                __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(2586, 2, true);
                WriteLiteral("\r\n");
                EndContext();
#line 71 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\Index.cshtml"
                        }

#line default
#line hidden
                BeginContext(2615, 24, true);
                WriteLiteral("                        ");
                EndContext();
                BeginContext(2639, 109, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("button", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "28954954e3214ec0990cb82036c0e19f", async() => {
                    BeginContext(2733, 6, true);
                    WriteLiteral("Delete");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_FormActionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormActionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormActionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
                __Microsoft_AspNetCore_Mvc_TagHelpers_FormActionTagHelper.PageHandler = (string)__tagHelperAttribute_4.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
                if (__Microsoft_AspNetCore_Mvc_TagHelpers_FormActionTagHelper.RouteValues == null)
                {
                    throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.FormActionTagHelper", "RouteValues"));
                }
                BeginWriteTagHelperAttribute();
#line 72 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\Index.cshtml"
                                                                                                WriteLiteral(item.Id);

#line default
#line hidden
                __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
                __Microsoft_AspNetCore_Mvc_TagHelpers_FormActionTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_FormActionTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(2748, 52, true);
                WriteLiteral("\r\n                    </td>\r\n                </tr>\r\n");
                EndContext();
#line 75 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\Index.cshtml"
            }

#line default
#line hidden
                BeginContext(2815, 32, true);
                WriteLiteral("        </tbody>\r\n    </table>\r\n");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_5.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2854, 2, true);
            WriteLiteral("\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IndexModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<IndexModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<IndexModel>)PageContext?.ViewData;
        public IndexModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
