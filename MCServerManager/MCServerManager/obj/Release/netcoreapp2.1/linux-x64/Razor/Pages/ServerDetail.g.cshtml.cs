#pragma checksum "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\ServerDetail.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0b264cd6938552998a248cf2e125986bb8f1dffb"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(MCServerManager.Pages.Pages_ServerDetail), @"mvc.1.0.razor-page", @"/Pages/ServerDetail.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.RazorPages.Infrastructure.RazorPageAttribute(@"/Pages/ServerDetail.cshtml", typeof(MCServerManager.Pages.Pages_ServerDetail), null)]
namespace MCServerManager.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\_ViewImports.cshtml"
using MCServerManager;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0b264cd6938552998a248cf2e125986bb8f1dffb", @"/Pages/ServerDetail.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"21e20d1a8d31c90f78c5a9c10380ac8a7726ef43", @"/Pages/_ViewImports.cshtml")]
    public class Pages_ServerDetail : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", new global::Microsoft.AspNetCore.Html.HtmlString("submit"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-page-handler", "Start", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("enctype", new global::Microsoft.AspNetCore.Html.HtmlString("multipart/form-data"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("action", new global::Microsoft.AspNetCore.Html.HtmlString("/Upload"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/serverDetails.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormActionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormActionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(33, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 4 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\ServerDetail.cshtml"
  
    ViewData["Title"] = "ServerDetail";

#line default
#line hidden
            BeginContext(83, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 8 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\ServerDetail.cshtml"
  
    string green = "#4cff00";
    string red = "#ff0000";
    var active = Model.IsActive;
    string color = (active) ? green : red;

#line default
#line hidden
            BeginContext(230, 5, true);
            WriteLiteral("<span");
            EndContext();
            BeginWriteAttribute("style", " style=\"", 235, "\"", 338, 11);
            WriteAttributeValue("", 243, "height:", 243, 7, true);
            WriteAttributeValue(" ", 250, "25px;", 251, 6, true);
            WriteAttributeValue(" ", 256, "width:", 257, 7, true);
            WriteAttributeValue(" ", 263, "25px;", 264, 6, true);
            WriteAttributeValue(" ", 269, "background-color:", 270, 18, true);
#line 14 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\ServerDetail.cshtml"
WriteAttributeValue(" ", 287, color, 288, 6, false);

#line default
#line hidden
            WriteAttributeValue("", 294, ";", 294, 1, true);
            WriteAttributeValue(" ", 295, "border-radius:", 296, 15, true);
            WriteAttributeValue(" ", 310, "50%;", 311, 5, true);
            WriteAttributeValue(" ", 315, "display:", 316, 9, true);
            WriteAttributeValue(" ", 324, "inline-block;", 325, 14, true);
            EndWriteAttribute();
            BeginContext(339, 71, true);
            WriteLiteral(" id=\"statusSymbol\"></span>\r\n<h2 style=\"display: inline-block;\">Server: ");
            EndContext();
            BeginContext(411, 19, false);
#line 15 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\ServerDetail.cshtml"
                                      Write(Model.MCServer.Name);

#line default
#line hidden
            EndContext();
            BeginContext(430, 9, true);
            WriteLiteral("</h2>\r\n\r\n");
            EndContext();
            BeginContext(439, 188, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "9f688c4a41ca4fbaa29ae29a3ea49785", async() => {
                BeginContext(459, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
                BeginContext(465, 95, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("button", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "4bf6cb20d53d48cd94e29ad4d53d2a3d", async() => {
                    BeginContext(546, 5, true);
                    WriteLiteral("Start");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_FormActionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormActionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormActionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                __Microsoft_AspNetCore_Mvc_TagHelpers_FormActionTagHelper.PageHandler = (string)__tagHelperAttribute_1.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
                if (__Microsoft_AspNetCore_Mvc_TagHelpers_FormActionTagHelper.RouteValues == null)
                {
                    throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.FormActionTagHelper", "RouteValues"));
                }
                BeginWriteTagHelperAttribute();
#line 18 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\ServerDetail.cshtml"
                                                     WriteLiteral(Model.MCServer.Id);

#line default
#line hidden
                __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
                __Microsoft_AspNetCore_Mvc_TagHelpers_FormActionTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_FormActionTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(560, 60, true);
                WriteLiteral("\r\n    <input type=\"button\" id=\"stopButton\" value=\"Stop\" />\r\n");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(627, 93, true);
            WriteLiteral("\r\n\r\n\r\n<button class=\"collapsible\"><h4>More Details</h4></button>\r\n<div class=\"content\">\r\n    ");
            EndContext();
            BeginContext(720, 555, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b739633e1b80465bbbf9d91e4b68a961", async() => {
                BeginContext(787, 213, true);
                WriteLiteral("\r\n        <div>\r\n            <p>Upload one or more files using this form:</p>\r\n            <input type=\"hidden\" name=\"filename\" id=\"FileName\" value=\"\" />\r\n            <input type=\"hidden\" name=\"folder\" id=\"Folder\"");
                EndContext();
                BeginWriteAttribute("value", " value=\"", 1000, "\"", 1030, 1);
#line 29 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\ServerDetail.cshtml"
WriteAttributeValue("", 1008, Model.MCServer.Folder, 1008, 22, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginContext(1031, 237, true);
                WriteLiteral(" />\r\n            <input type=\"file\" name=\"files\" accept=\".zip\" onchange=\"document.getElementById(\'FileName\').value = this.value;\" />\r\n        </div>\r\n        <div>\r\n            <input type=\"submit\" value=\"Upload\" />\r\n        </div>\r\n    ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1275, 72, true);
            WriteLiteral("\r\n    <hr />\r\n    <dl class=\"dl-horizontal\">\r\n        <dt>\r\n            ");
            EndContext();
            BeginContext(1348, 49, false);
#line 39 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\ServerDetail.cshtml"
       Write(Html.DisplayNameFor(model => model.MCServer.Name));

#line default
#line hidden
            EndContext();
            BeginContext(1397, 43, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(1441, 45, false);
#line 42 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\ServerDetail.cshtml"
       Write(Html.DisplayFor(model => model.MCServer.Name));

#line default
#line hidden
            EndContext();
            BeginContext(1486, 43, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt>\r\n            ");
            EndContext();
            BeginContext(1530, 52, false);
#line 45 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\ServerDetail.cshtml"
       Write(Html.DisplayNameFor(model => model.MCServer.Modpack));

#line default
#line hidden
            EndContext();
            BeginContext(1582, 43, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(1626, 48, false);
#line 48 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\ServerDetail.cshtml"
       Write(Html.DisplayFor(model => model.MCServer.Modpack));

#line default
#line hidden
            EndContext();
            BeginContext(1674, 43, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt>\r\n            ");
            EndContext();
            BeginContext(1718, 49, false);
#line 51 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\ServerDetail.cshtml"
       Write(Html.DisplayNameFor(model => model.MCServer.Port));

#line default
#line hidden
            EndContext();
            BeginContext(1767, 43, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(1811, 45, false);
#line 54 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\ServerDetail.cshtml"
       Write(Html.DisplayFor(model => model.MCServer.Port));

#line default
#line hidden
            EndContext();
            BeginContext(1856, 43, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt>\r\n            ");
            EndContext();
            BeginContext(1900, 51, false);
#line 57 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\ServerDetail.cshtml"
       Write(Html.DisplayNameFor(model => model.MCServer.MaxRam));

#line default
#line hidden
            EndContext();
            BeginContext(1951, 43, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(1995, 47, false);
#line 60 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\ServerDetail.cshtml"
       Write(Html.DisplayFor(model => model.MCServer.MaxRam));

#line default
#line hidden
            EndContext();
            BeginContext(2042, 43, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt>\r\n            ");
            EndContext();
            BeginContext(2086, 48, false);
#line 63 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\ServerDetail.cshtml"
       Write(Html.DisplayNameFor(model => model.MCServer.Jar));

#line default
#line hidden
            EndContext();
            BeginContext(2134, 43, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(2178, 44, false);
#line 66 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\ServerDetail.cshtml"
       Write(Html.DisplayFor(model => model.MCServer.Jar));

#line default
#line hidden
            EndContext();
            BeginContext(2222, 43, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt>\r\n            ");
            EndContext();
            BeginContext(2266, 51, false);
#line 69 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\ServerDetail.cshtml"
       Write(Html.DisplayNameFor(model => model.MCServer.Folder));

#line default
#line hidden
            EndContext();
            BeginContext(2317, 43, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(2361, 47, false);
#line 72 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\ServerDetail.cshtml"
       Write(Html.DisplayFor(model => model.MCServer.Folder));

#line default
#line hidden
            EndContext();
            BeginContext(2408, 119, true);
            WriteLiteral("\r\n        </dd>\r\n    </dl>\r\n</div>\r\n<div id=\"serverOutputArea\" style=\"overflow: auto; height: 500px; width:inherit;\">\r\n");
            EndContext();
#line 77 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\ServerDetail.cshtml"
     foreach (var line in Model.MCServer.Log)
    {

#line default
#line hidden
            BeginContext(2581, 16, true);
            WriteLiteral("        <br />\r\n");
            EndContext();
            BeginContext(2606, 4, false);
#line 80 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\ServerDetail.cshtml"
   Write(line);

#line default
#line hidden
            EndContext();
#line 80 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\MCServerManager\MCServerManager\Pages\ServerDetail.cshtml"
             
    }

#line default
#line hidden
            BeginContext(2619, 498, true);
            WriteLiteral(@"</div>
<div>
    <div class=""container"">
        <div class=""row"">&nbsp;</div>
        <div class=""row"">
            <div class=""col-10"">&nbsp;</div>
            <div class=""col-10"">
                <label style=""display: inline-block; padding-right: 5px"">Command:</label><input style=""display: inline-block; width: 500px"" type=""text"" id=""commandInput"" />
                <input type=""button"" id=""sendButton"" value=""Send Command"" />
            </div>
        </div>
    </div>
</div>
");
            EndContext();
            BeginContext(3117, 70, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "2a5fa90c94eb444fa921dde67d18553b", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            BeginWriteTagHelperAttribute();
            WriteLiteral("~/lib/@");
            WriteLiteral("aspnet/signalr/dist/browser/signalr.js");
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __tagHelperExecutionContext.AddHtmlAttribute("src", Html.Raw(__tagHelperStringValueBuffer), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(3187, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(3189, 45, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b2b702412efb4319b57d5200babfbd9f", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<ServerDetailModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<ServerDetailModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<ServerDetailModel>)PageContext?.ViewData;
        public ServerDetailModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
