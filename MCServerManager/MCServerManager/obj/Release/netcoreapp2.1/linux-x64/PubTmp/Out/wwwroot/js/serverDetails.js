﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/ServerOutput").build();
//var model = @Html.Raw(Json.Encode(Model));

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}

var myId = getUrlVars()["id"];

connection.on("ReceiveMessage", function (message) {
    var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    var outputDiv = document.getElementById("serverOutputArea");
    outputDiv.innerHTML += "<br />";
    outputDiv.append(msg);
    outputDiv.scrollTop = outputDiv.scrollHeight;


});

connection.start().then(() => {
    connection.invoke("JoinGroup", myId).catch(function (err) {
        return console.error(err.toString());
    });
}).catch(function (err) {
    return console.error(err.toString());
    });


document.getElementById("sendButton").addEventListener("click", function (event) {
    var command = document.getElementById("commandInput").value;
    connection.invoke("SendCommand", myId, command).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});




