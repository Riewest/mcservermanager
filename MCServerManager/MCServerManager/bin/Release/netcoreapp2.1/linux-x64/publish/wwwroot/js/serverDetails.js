﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/ServerOutput").build();

var maxHist = 10;
var commandHistory = [];
var commandHistory2 = [];
var currenthist = 0;
var storedHist

function setCommand() {
    var cmd = commandHistory[commandHistory.length - 1];
    document.getElementById("commandInput").value = cmd;
}

function incrementCommand() {
    console.log("INC");
    if (commandHistory.length <= 0) {
        return
    } else if (commandHistory.length === 1) {
        setCommand();
        return;
    }
    setCommand();
    commandHistory2.push(commandHistory.pop());    
}

function decrementCommand() {
    console.log("DEC");
    if (commandHistory2.length <= 0) {
        document.getElementById("commandInput").value = '';
        return
    }
    commandHistory.push(commandHistory2.pop());
    setCommand();
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}

function sendCommand() {
    var command = document.getElementById("commandInput").value;
    connection.invoke("SendCommand", myId, command).catch(function (err) {
        return console.error(err.toString());
    });    
    document.getElementById("commandInput").value = '';
    if (!commandHistory.includes(command) && command !== '') {
        commandHistory.push(command);
    }
    var time = new Date();
    command = "[" + time.toTimeString().split(' ')[0] + "]" + " [Execute Command]: " + command;
    var outputDiv = document.getElementById("serverOutputArea");
    outputDiv.innerHTML += "<br />";
    outputDiv.append(command);
    outputDiv.scrollTop = outputDiv.scrollHeight;
}

var myId = getUrlVars()["id"];

connection.on("ReceiveMessage", function (message) {
    var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    var outputDiv = document.getElementById("serverOutputArea");
    outputDiv.innerHTML += "<br />";
    outputDiv.append(msg);
    outputDiv.scrollTop = outputDiv.scrollHeight;


});

connection.start().then(() => {
    connection.invoke("JoinGroup", myId).catch(function (err) {
        return console.error(err.toString());
    });
}).catch(function (err) {
    return console.error(err.toString());
    });

document.getElementById("commandInput").addEventListener("keyup", function (event) {
    event.preventDefault();
    switch (event.keyCode) {
        case 13:
            sendCommand();
            break;
        case 38:
            incrementCommand();
            break;
        case 40:
            decrementCommand();
            break;
        default:
            break;
    }
})

document.getElementById("sendButton").addEventListener("click", function (event) {
    sendCommand();
    event.preventDefault();
});

document.getElementById("stopButton").addEventListener("click", function (event) {
    connection.invoke("StopServer", myId).catch(function (err) {
        return console.error(err.toString());
    });
    document.getElementById("statusSymbol").style.backgroundColor = "red";
    event.preventDefault();
});


//SCROLL TO BOTTOM ON PAGE LOAD
var outputDiv = document.getElementById("serverOutputArea");
outputDiv.scrollTop = outputDiv.scrollHeight;