﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MCServerManager
{
    public class MCServerContext
    {
        public string ConnectionString { get; set; }

        public MCServerContext(string connectionString)
        {
            ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }

        public void AddMCServer(MCServer server)
        {
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = conn.CreateCommand();
                if(string.IsNullOrEmpty(server.Modpack))
                {
                    cmd.CommandText = "INSERT INTO Servers (NAME, PORT, MAXRAM, JAR, FOLDER) VALUES (?NAME, ?PORT, ?MAXRAM, ?JAR, ?FOLDER)";
                }
                else
                {
                    cmd.CommandText = "INSERT INTO Servers (NAME, MODPACK, PORT, MAXRAM, JAR, FOLDER) VALUES (?NAME, ?MODPACK, ?PORT, ?MAXRAM, ?JAR, ?FOLDER)";
                    cmd.Parameters.Add("?MODPACK", MySqlDbType.VarChar).Value = server.Modpack;
                }
                cmd.Parameters.Add("?NAME", MySqlDbType.VarChar).Value = server.Name;
                cmd.Parameters.Add("?PORT", MySqlDbType.Int32).Value = server.Port;
                cmd.Parameters.Add("?MAXRAM", MySqlDbType.Int32).Value = server.MaxRam;
                cmd.Parameters.Add("?JAR", MySqlDbType.VarChar).Value = server.Jar;
                cmd.Parameters.Add("?FOLDER", MySqlDbType.VarChar).Value = server.Folder;
                cmd.Transaction = conn.BeginTransaction();
                try
                {
                    cmd.ExecuteNonQuery();
                    cmd.Transaction.Commit();
                }
                catch (Exception e)
                {

                    cmd.Transaction.Rollback();
                }
                conn.Close();
            }
        }

        public void UpdateMCServer(MCServer server)
        {
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = conn.CreateCommand();
                
                if (string.IsNullOrEmpty(server.Modpack))
                {
                    cmd.CommandText = "UPDATE Servers SET NAME=?NAME,PORT=?PORT,MAXRAM=?MAXRAM,JAR=?JAR,FOLDER=?FOLDER WHERE ID = ?ID;";
                }
                else
                {
                    cmd.CommandText = "UPDATE Servers SET NAME = ?NAME, MODPACK = ?MODPACK, PORT = ?PORT, MAXRAM = ?MAXRAM, JAR = ?JAR, FOLDER = ?FOLDER WHERE ID = ?ID;";
                    cmd.Parameters.Add("?MODPACK", MySqlDbType.VarChar).Value = server.Modpack;
                }
                cmd.Parameters.Add("?ID", MySqlDbType.Int32).Value = server.Id;
                cmd.Parameters.Add("?NAME", MySqlDbType.VarChar).Value = server.Name;
                cmd.Parameters.Add("?PORT", MySqlDbType.Int32).Value = server.Port;
                cmd.Parameters.Add("?MAXRAM", MySqlDbType.Int32).Value = server.MaxRam;
                cmd.Parameters.Add("?JAR", MySqlDbType.VarChar).Value = server.Jar;
                cmd.Parameters.Add("?FOLDER", MySqlDbType.VarChar).Value = server.Folder;
                cmd.Transaction = conn.BeginTransaction();
                try
                {
                    cmd.ExecuteNonQuery();
                    cmd.Transaction.Commit();
                }
                catch (Exception e)
                {

                    cmd.Transaction.Rollback();
                }
                conn.Close();
            }
        }

        public MCServer FindMCServer(int id)
        {
            MCServer server = new MCServer();
            server.Id = id;
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "select * from Servers where ID = ?ID";
                cmd.Parameters.Add("?ID", MySqlDbType.Int32).Value = server.Id;
                var reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    server.Name = reader["NAME"].ToString();
                    server.Modpack = reader["MODPACK"].ToString();
                    server.Port = Convert.ToInt32(reader["PORT"]);
                    server.MaxRam = Convert.ToInt32(reader["MAXRAM"]);
                    server.Jar = reader["JAR"].ToString();
                    server.Folder = reader["FOLDER"].ToString();
                }
                conn.Close();
            }
            return server;
        }

        public void RemoveMCServer(int id)
        {
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "DELETE FROM Servers WHERE ID = ?ID;";
                cmd.Parameters.Add("?ID", MySqlDbType.Int32).Value = id;
                cmd.Transaction = conn.BeginTransaction();
                try
                {
                    cmd.ExecuteNonQuery();
                    cmd.Transaction.Commit();
                }
                catch (Exception e)
                {

                    cmd.Transaction.Rollback();
                }
                conn.Close();
            }
        }

        public List<MCServer> GetAllMCServers()
        {
            List<MCServer> customers = new List<MCServer>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("select * from Servers", conn);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        customers.Add(new MCServer
                        {
                            Id = Convert.ToInt32(reader["ID"]),
                            Name = reader["NAME"].ToString(),
                            Modpack = reader["MODPACK"].ToString(),
                            Port = Convert.ToInt32(reader["PORT"]),
                            MaxRam = Convert.ToInt32(reader["MAXRAM"]),
                            Jar = reader["JAR"].ToString(),
                            Folder = reader["FOLDER"].ToString()
                        });
                    }
                }
                conn.Close();

            }
            return customers;
        }
    }
}
