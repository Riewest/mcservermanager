﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MCServerManager
{
    public class SettingsContext : DbContext
    {
        public SettingsContext (DbContextOptions<SettingsContext> options)
            : base(options)
        {
        }

        public DbSet<MCServerManager.Settings> Settings { get; set; }
    }
}
