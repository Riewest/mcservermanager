﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MCServerManager.Hubs
{
    public class ServerHub : Hub
    {
        public void JoinGroup(string group)
        {
            Groups.AddToGroupAsync(Context.ConnectionId, group);
        }

        public Task SendMessageToGroup(string message, string group)
        {
            return Clients.Group(group).SendAsync("RecieveMessage", message);
        }

        public async Task SendCommand(string id, string command)
        {
            MCServerManagerClass.Instance.SendCommandById(Convert.ToInt32(id), command);
        }

        public async Task StopServer(string id)
        {
            MCServerManagerClass.Instance.StopServerById(Convert.ToInt32(id));
        }

        public async Task RestartServer(string id)
        {
            await MCServerManagerClass.Instance.RestartServerById(Convert.ToInt32(id));
        }
    }
}
