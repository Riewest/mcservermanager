﻿using MCServerManager.Hubs;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MCServerManager
{
    public class MCServer
    {
        private static readonly string LOG = @"/logs/serverLatest.log";
        private static readonly string PROP = @"/server.properties";

        public int Id { get; set; }

        [Required, StringLength(100)]
        public string Name { get; set; }

        [StringLength(100)]
        public string Modpack { get; set; }

        [Required]
        public int Port { get; set; }

        [Required, Range(0, 14336)] // Maximum of 14 Gb of Ram (14 * 1024 = 14366)
        public int MaxRam { get; set; }

        [Required, StringLength(100)]
        public string Jar { get; set; }

        [Required, StringLength(100)]
        public string Folder { get; set; }

        public IHubContext<ServerHub> _serverHub { get; set; }

        public bool IsActive { get; set; }

        public bool CreateDirectory { get; set; }

        private Process Minecraft;

        public MCServer() { IsActive = false; }

        public MCServer(IHubContext<ServerHub> serverHub) : base()
        {
            _serverHub = serverHub;
        }

        public List<string> Log
        {
            get
            {
                string path = Folder + LOG;
                if (File.Exists(path))
                {
                    var lines = File.ReadLines(path, Encoding.Unicode).ToList();
                    return lines;
                }
                return new List<string>();
            }
        }

        public void Start()
        {
            if (_serverHub == null)
            {
                throw new Exception("No Server Hub Stated");
            }
            CheckForFile();
            CheckProperties();
            Console.WriteLine($"Starting Server: {Name}");
            string startParams = $"-Xmx{MaxRam.ToString()}M -Xms{MaxRam.ToString()}M -jar {Jar} nogui";
            ProcessStartInfo startInfo = new ProcessStartInfo("java", startParams);
            // Replace the following with the location of your Minecraft Server
            startInfo.WorkingDirectory = Folder;
            // Notice that the Minecraft Server uses the Standard Error instead of the Standard Output
            startInfo.RedirectStandardInput = startInfo.RedirectStandardError = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.UseShellExecute = false; // Necessary for Standard Stream Redirection
            startInfo.CreateNoWindow = true; // You can do either this or open it with "javaw" instead of "java"
            Minecraft = new Process();
            Minecraft.StartInfo = startInfo;
            Minecraft.EnableRaisingEvents = true;
            Minecraft.OutputDataReceived += Minecraft_OutputDataReceived;
            Minecraft.ErrorDataReceived += Minecraft_ErrorDataReceived;
            Minecraft.Start();
            Minecraft.BeginOutputReadLine();
            Minecraft.BeginErrorReadLine();
            IsActive = true;
        }

        public void InputCommand(string command = "")
        {
            try
            {
                if (!IsActive)
                    return;
                if (string.IsNullOrEmpty(command))
                    return;
                Minecraft.StandardInput.WriteLineAsync(command);
            }
            catch { }
        }

        public void Close()
        {
            Console.WriteLine($"Stopping Server: {Name}");
            Minecraft.StandardInput.WriteLineAsync("stop");
            Thread.Sleep(500);
            Minecraft.Close();
            IsActive = false;
        }

        private void Minecraft_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            _serverHub.Clients.Group(Id.ToString()).SendAsync("ReceiveMessage", e.Data);
            ProcessWrite(e.Data).Wait();
        }

        private void Minecraft_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            _serverHub.Clients.Group(Id.ToString()).SendAsync("ReceiveMessage", e.Data);
            ProcessWrite(e.Data).Wait();
        }

        private void CheckForFile()
        {
            string path = Folder + LOG;
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }

        private Task ProcessWrite(string command)
        {
            string filePath = Folder + LOG;

            return WriteTextAsync(filePath, command);
        }

        private static async Task WriteTextAsync(string filePath, string text)
        {
            if (!Directory.Exists(Path.GetDirectoryName(filePath)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(filePath));
            }
            byte[] encodedText = Encoding.Unicode.GetBytes(text + "\r\n");

            using (FileStream sourceStream = new FileStream(filePath,
                FileMode.Append, FileAccess.Write, FileShare.None,
                bufferSize: 4096, useAsync: true))
            {
                await sourceStream.WriteAsync(encodedText, 0, encodedText.Length);
            };
        }

        public static void MakeDirectory(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        private void CheckProperties()
        {
            string serverLine = "server-port=";
            string path = Folder + PROP;
            if (File.Exists(path))
            {
                var lines = File.ReadLines(path).ToList();
                var index = lines.FindIndex(l => l.Contains(serverLine));
                if (!lines[index].Equals(serverLine + Port.ToString()))
                {
                    lines[index] = serverLine + Port.ToString();
                    File.WriteAllLines(path, lines);
                }
            }
        }


    }
}
