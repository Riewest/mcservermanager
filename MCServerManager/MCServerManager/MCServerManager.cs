﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MCServerManager
{
    public class MCServerManagerClass
    {
        private static MCServerManagerClass instance = null;

        private static List<MCServer> Servers;

        private MCServerManagerClass()
        {
            Servers = new List<MCServer>();
        }

        public static MCServerManagerClass Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MCServerManagerClass();
                }
                return instance;
            }
        }

        public MCServer GetServerById(int id)
        {
            if (Servers.Exists(x => x.Id == id))
            {
                return Servers.Find(x => x.Id == id);
            }
            return null;
        }

        public void AddServer(MCServer server)
        {
            if(!Servers.Exists(x => x.Id == server.Id))
            {
                Servers.Add(server);
            }
        }

        public void UpdateServer(MCServer server)
        {
            if (Servers.Exists(x => x.Id == server.Id))
            {
                var index = Servers.FindIndex(s => s.Id == server.Id);
                Servers[index].Port = server.Port;
            }
        }

        public void StartServerById(int id)
        {
            if(Servers.Exists(x => x.Id == id))
            {
                MCServer server = Servers.Find(x => x.Id == id);
                if (!server.IsActive)
                {
                    server.Start();
                }
            }
                
        }

        public void StopServerById(int id)
        {
            if (Servers.Exists(x => x.Id == id))
            {
                MCServer server = Servers.Find(x => x.Id == id);
                if (server.IsActive)
                {
                    server.Close();
                }
            }
        }

        public async Task RestartServerById(int id)
        {
            if (Servers.Exists(x => x.Id == id))
            {
                MCServer server = Servers.Find(x => x.Id == id);
                if (server.IsActive)
                {
                    server.Close();
                    Thread.Sleep(5000);
                    server.Start();
                }
            }
        }

        public void SendCommandById(int id, string command)
        {
            if (Servers.Exists(x => x.Id == id))
            {
                Servers.Find(x => x.Id == id).InputCommand(command);
            }
        }

        public List<MCServer> GetActiveServers()
        {
            return Servers.FindAll(x => x.IsActive);
        }

        public bool GetActiveById(int id)
        {
            if (Servers.Exists(x => x.Id == id))
            {
                return Servers.Find(x => x.Id == id).IsActive;
            }
            return false;
        }
    }
}
