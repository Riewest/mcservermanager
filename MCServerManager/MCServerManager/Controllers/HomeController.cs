﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using MCServerManager.Pages;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace MCServerManager.Controllers
{
    public class HomeController : Controller
    {
        
        private static readonly string tempFilePath = Path.Combine(Directory.GetCurrentDirectory(), "myfile.temp");

        //[DisableFormValueModelBinding]
        //private T Upload<T>(ref T viewModel)
        //{
        //    FormValueProvider formModel;
        //    using (var stream = System.IO.File.Create(tempFilePath))
        //    {
        //        formModel = Task.Run(() => Request.StreamFile(stream)).Result;
        //    }

        //    var tempModel = viewModel;

        //    var bindingSuccessful = Task.Run(() => TryUpdateModelAsync(tempModel, prefix: "",
        //       valueProvider: formModel)).Result;
            
        //    if (!bindingSuccessful)
        //    {
        //        if (!ModelState.IsValid)
        //        {
        //            return default(T);
        //        }
        //    }

        //    return viewModel;
        //}

        //[HttpPost]
        //[DisableRequestSizeLimit]
        //[DisableFormValueModelBinding]
        //public async Task<IActionResult> UploadServerZip()
        //{
        //    ServerDetailModel viewModel = new ServerDetailModel(null, null);
        //    viewModel = Upload(ref viewModel);

        //    if(viewModel == null)
        //    {
        //        return BadRequest();
        //    }

        //    string filename = Path.GetFileName(viewModel.Filename);
        //    string folder = viewModel.Folder;
        //    string filepath = Path.Combine(folder, filename);

        //    if (!System.IO.File.Exists(filepath))
        //    {
        //        System.IO.File.Move(tempFilePath, filepath);
        //    }


        //    if (Path.GetExtension(filepath) == ".zip")
        //    {
        //        ZipFile.ExtractToDirectory(filepath, folder, true);
        //        System.IO.File.Delete(filepath);
        //    }

        //    if (System.IO.File.Exists(tempFilePath))
        //    {
        //        System.IO.File.Delete(tempFilePath);
        //    }

        //    return Redirect($"/ServerDetail?id={viewModel.ServerId.ToString()}");
        //}

            public async Task<IActionResult> Download(string folder, string filename, string id)
        {
            if (folder == null || filename == null)
                return Redirect($"/ServerDetail?id={id}");
            var filepath = folder + filename;

            if(!System.IO.File.Exists(filepath))
                return Redirect($"/ServerDetail?id={id}");

            var memory = new MemoryStream();
            using (var stream = new FileStream(filepath, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, GetContentType(filepath), Path.GetFileName(filepath));
        }

        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".log", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }
    }
}