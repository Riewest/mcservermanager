﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Text;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;


namespace ConsoleApp1
{
    public class MinecraftServer
    {
        private static string ServerFile = "server.jar";
        private static string ServerPath = "C:\\Users\\chase\\Desktop\\Minecraft";


        public int Id { get; set; }

        public MinecraftServer(int id)
        {
            Id = id;
        }

        Process Minecraft;

        public void Start()
        {
            Console.WriteLine("Starting Server");
            ProcessStartInfo startInfo = new ProcessStartInfo("java", "-Xmx1024M -Xms1024M -jar " + ServerFile + " nogui");
            // Replace the following with the location of your Minecraft Server
            startInfo.WorkingDirectory = ServerPath;
            // Notice that the Minecraft Server uses the Standard Error instead of the Standard Output
            startInfo.RedirectStandardInput = startInfo.RedirectStandardError = true;
            //startInfo.RedirectStandardOutput = true;
            startInfo.UseShellExecute = false; // Necessary for Standard Stream Redirection
            startInfo.CreateNoWindow = true; // You can do either this or open it with "javaw" instead of "java"
            Minecraft = new Process();
            Minecraft.StartInfo = startInfo;
            Minecraft.EnableRaisingEvents = true;
            //Minecraft.OutputDataReceived += Minecraft_OutputDataReceived;
           // Minecraft.ErrorDataReceived += Minecraft_ErrorDataReceived;
            Minecraft.Start();
            //Minecraft.BeginOutputReadLine();
            //Minecraft.BeginErrorReadLine();
        }

        public void InputCommand(string command = "")
        {
            try
            {
                Minecraft.StandardInput.WriteLineAsync(command);
            }
            catch { }
        }

        public void Close()
        {
            Minecraft.StandardInput.WriteLineAsync("stop");
            Minecraft.Close();
        }

        public void GetLog()
        {
            ServerLog log = new ServerLog();
            log.ReadLog(ServerPath);
        }

        void Minecraft_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            Console.WriteLine(e.Data);
        }

        void Minecraft_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            try
            {
                Console.WriteLine(e.Data);
            }
            catch (Exception ex)
            {
            }
        }
    }
}
