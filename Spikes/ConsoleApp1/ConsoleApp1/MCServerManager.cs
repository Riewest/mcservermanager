﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class MCServerManager
    {
        private static MCServerManager instance = null;

        private static List<MinecraftServer> Servers;

        private MCServerManager()
        {
            Servers = new List<MinecraftServer>();
        }

        public static MCServerManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MCServerManager();
                }
                return instance;
            }
        }

        public MinecraftServer GetServerById(int id)
        {
            return Servers.Find(x => x.Id == id);
        }

        public void AddServer(MinecraftServer server)
        {
            Servers.Add(server);
        }

        public void StartServerById(int id)
        {
            Servers.Find(x => x.Id == id).Start();
        }

        public void StopServerById(int id)
        {
            Servers.Find(x => x.Id == id).Start();
        }
    }
}
