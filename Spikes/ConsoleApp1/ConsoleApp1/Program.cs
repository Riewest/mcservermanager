﻿using System;
using System.Threading;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This Is Minecraft!");

            MinecraftServer server = new MinecraftServer(1);

            MCServerManager.Instance.AddServer(server);
            MCServerManager.Instance.GetServerById(1).Start();
            //Console.WriteLine("Press any key to continue.");
            //Console.ReadKey();
            MCServerManager.Instance.GetServerById(1).Close();
        }
    }
}
