﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ConsoleApp1
{
    public class ServerLog
    {
        public int RowCount { get; set; }

        public string Log { get; set; }


        public void ReadLog(string path)
        {
            RowCount = 0;
            List<string> log = new List<string>();
            path += @"\logs\latest.log";
            using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                using (var sr = new StreamReader(fs, Encoding.Default))
                {
                    
                    string line;
                    while ( (line = sr.ReadLine()) != null)
                    {
                        RowCount++;
                        log.Add(line);
                    }
                }
            }
            Log = string.Join("", log.ToArray());
        }
    }
}
