﻿using sqlite.Shared;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sqlite.Server.Controllers
{
    [Route("api/[controller]")]
    public class SampleDataController : Controller
    {
        private readonly DatabaseContext _db;

        public SampleDataController(DatabaseContext db)
        {
            _db = db;
        }

        private static string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        [HttpGet("[action]")]
        public IEnumerable<WeatherForecast> WeatherForecasts()
        {
            List<WeatherForecast> test = _db.weatherForecasts.ToList();
            return test;
        }

        [HttpPost("[action]")]
        public void SaveForecasts([FromBody] WeatherForecast newForecast)
        {
            _db.weatherForecasts.Add(newForecast);
            _db.SaveChanges();
        }
    }
}
