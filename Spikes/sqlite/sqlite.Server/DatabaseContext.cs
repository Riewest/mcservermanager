﻿using Microsoft.EntityFrameworkCore;
using sqlite.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sqlite.Server
{
    public class DatabaseContext : DbContext
    {
        public DbSet<WeatherForecast> weatherForecasts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=MyDatabase.db");
        }
    }
}
