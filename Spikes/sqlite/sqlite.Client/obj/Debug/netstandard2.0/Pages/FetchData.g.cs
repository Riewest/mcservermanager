#pragma checksum "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\Spikes\sqlite\sqlite.Client\Pages\FetchData.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "361290886e9e9798492598011781e6e5381255bc"
// <auto-generated/>
#pragma warning disable 1591
namespace sqlite.Client.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Blazor;
    using Microsoft.AspNetCore.Blazor.Components;
    using System.Net.Http;
    using Microsoft.AspNetCore.Blazor.Layouts;
    using Microsoft.AspNetCore.Blazor.Routing;
    using Microsoft.JSInterop;
    using sqlite.Client;
    using sqlite.Client.Shared;
    using sqlite.Shared;
    [Microsoft.AspNetCore.Blazor.Layouts.LayoutAttribute(typeof(MainLayout))]

    [Microsoft.AspNetCore.Blazor.Components.RouteAttribute("/fetchdata")]
    public class FetchData : Microsoft.AspNetCore.Blazor.Components.BlazorComponent
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Blazor.RenderTree.RenderTreeBuilder builder)
        {
            base.BuildRenderTree(builder);
            builder.AddMarkupContent(0, "<h1>Weather forecast</h1>\n\n");
            builder.AddMarkupContent(1, "<p>This component demonstrates fetching data from the server.</p>\n");
            builder.OpenElement(2, "input");
            builder.AddAttribute(3, "type", "button");
            builder.AddAttribute(4, "onclick", Microsoft.AspNetCore.Blazor.Components.BindMethods.GetEventHandlerValue<Microsoft.AspNetCore.Blazor.UIMouseEventArgs>(SaveForecasts));
            builder.AddAttribute(5, "value", "SAVE FORECASTS");
            builder.CloseElement();
            builder.AddContent(6, "\n\n");
#line 10 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\Spikes\sqlite\sqlite.Client\Pages\FetchData.cshtml"
 if (forecasts == null)
{

#line default
#line hidden
            builder.AddContent(7, "    ");
            builder.OpenComponent<sqlite.Client.Components.Loading>(8);
            builder.CloseComponent();
            builder.AddContent(9, "\n");
#line 13 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\Spikes\sqlite\sqlite.Client\Pages\FetchData.cshtml"
}
else
{

#line default
#line hidden
            builder.AddContent(10, "    ");
            builder.OpenElement(11, "table");
            builder.AddAttribute(12, "class", "table");
            builder.AddContent(13, "\n        ");
            builder.AddMarkupContent(14, "<thead>\n            <tr>\n                <th>Date</th>\n                <th>Temp. (C)</th>\n                <th>Temp. (F)</th>\n                <th>Summary</th>\n            </tr>\n        </thead>\n        ");
            builder.OpenElement(15, "tbody");
            builder.AddContent(16, "\n");
#line 26 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\Spikes\sqlite\sqlite.Client\Pages\FetchData.cshtml"
             foreach (var forecast in forecasts)
            {

#line default
#line hidden
            builder.AddContent(17, "                ");
            builder.OpenElement(18, "tr");
            builder.AddContent(19, "\n                    ");
            builder.OpenElement(20, "td");
            builder.AddContent(21, forecast.Date.ToShortDateString());
            builder.CloseElement();
            builder.AddContent(22, "\n                    ");
            builder.OpenElement(23, "td");
            builder.AddContent(24, forecast.TemperatureC);
            builder.CloseElement();
            builder.AddContent(25, "\n                    ");
            builder.OpenElement(26, "td");
            builder.AddContent(27, forecast.TemperatureF);
            builder.CloseElement();
            builder.AddContent(28, "\n                    ");
            builder.OpenElement(29, "td");
            builder.AddContent(30, forecast.Summary);
            builder.CloseElement();
            builder.AddContent(31, "\n                ");
            builder.CloseElement();
            builder.AddContent(32, "\n");
#line 34 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\Spikes\sqlite\sqlite.Client\Pages\FetchData.cshtml"
            }

#line default
#line hidden
            builder.AddContent(33, "        ");
            builder.CloseElement();
            builder.AddContent(34, "\n    ");
            builder.CloseElement();
            builder.AddContent(35, "\n");
#line 37 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\Spikes\sqlite\sqlite.Client\Pages\FetchData.cshtml"
}

#line default
#line hidden
        }
        #pragma warning restore 1998
#line 39 "C:\Users\chase\Documents\Coding\WebDevelopment\MinecraftWrapper\Spikes\sqlite\sqlite.Client\Pages\FetchData.cshtml"
            
    WeatherForecast[] forecasts;

    protected override async Task OnInitAsync()
    {
        forecasts = await Http.GetJsonAsync<WeatherForecast[]>("api/SampleData/WeatherForecasts");
    }

    protected async void SaveForecasts()
    {
        var newForecast = new WeatherForecast
        {
            Id = 1,
            Date = DateTime.Now,
            TemperatureC = 5,
            Summary = "Fricken cold",

        };
        await Http.PostJsonAsync<WeatherForecast>("api/SampleData/SaveForecasts", newForecast);
    }

#line default
#line hidden
        [global::Microsoft.AspNetCore.Blazor.Components.InjectAttribute] private HttpClient Http { get; set; }
    }
}
#pragma warning restore 1591
