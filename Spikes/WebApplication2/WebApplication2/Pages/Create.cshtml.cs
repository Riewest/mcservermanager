﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApplication2.Pages
{
    public class CreateModel : PageModel
    {
        private readonly StoreContext _db;

        [TempData]
        public string Message { get; set; }

        [BindProperty]
        public Customer Customer { get; set; }

        public CreateModel(StoreContext db)
        {
            _db = db;
        }

        public IActionResult OnPost()
        {
            if(!ModelState.IsValid) { return Page(); }

            _db.AddCustomer(Customer);
            Message = $"Customer {Customer.Name} added!";
            return RedirectToPage("/Index");
        }
    }
}