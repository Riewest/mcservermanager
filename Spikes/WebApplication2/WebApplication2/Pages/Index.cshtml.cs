﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApplication2.Pages
{
    public class IndexModel : PageModel
    {

        private readonly StoreContext _db;

        public IList<Customer> Customers { get; private set; }

        [TempData]
        public string Message { get; set; }

        public IndexModel(StoreContext db)
        {
            _db = db;
        }

        public async Task OnGet()
        {
            Customers = _db.GetAllCustomers();
        }

        public IActionResult OnPostDelete(int id)
        {
            var customer =  _db.FindCustomer(id);
            if(customer.Name != null)
            {
                _db.RemoveCustomer(id);
            }
            return RedirectToPage();
        }
    }
}
