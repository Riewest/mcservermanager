﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace WebApplication2
{
    public class StoreContext
    {
        public string ConnectionString { get; set; }

        public StoreContext(string connectionString)
        {
            ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }

        public void AddCustomer(Customer customer)
        {
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "INSERT INTO TestTable1 (NAME) VALUES (?NAME)";
                cmd.Parameters.Add("?NAME", MySqlDbType.VarChar).Value = customer.Name;
                cmd.Transaction = conn.BeginTransaction();
                try
                {
                    cmd.ExecuteNonQuery();
                    cmd.Transaction.Commit();
                }
                catch (Exception e)
                {

                    cmd.Transaction.Rollback();
                }
                conn.Close();
            }
        }

        public void UpdateCustomer(Customer customer)
        {
            using(MySqlConnection conn = GetConnection())
            {
                conn.Open(); 
                MySqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE TestTable1   SET NAME = ?NAME WHERE ID = ?ID;";
                cmd.Parameters.Add("?NAME", MySqlDbType.VarChar).Value = customer.Name;
                cmd.Parameters.Add("?ID", MySqlDbType.Int32).Value = customer.Id;
                cmd.Transaction = conn.BeginTransaction();
                try
                {
                    cmd.ExecuteNonQuery();
                    cmd.Transaction.Commit();
                }
                catch (Exception e)
                {

                    cmd.Transaction.Rollback();
                }
                conn.Close();
            }
        }


        public void RemoveCustomer(int id)
        {
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "DELETE FROM TestTable1 WHERE ID = ?ID;";
                cmd.Parameters.Add("?ID", MySqlDbType.Int32).Value = id;
                cmd.Transaction = conn.BeginTransaction();
                try
                {
                    cmd.ExecuteNonQuery();
                    cmd.Transaction.Commit();
                }
                catch (Exception e)
                {

                    cmd.Transaction.Rollback();
                }
                conn.Close();
            }
        }

        public Customer FindCustomer(int id)
        {
            Customer customer = new Customer();
            customer.Id = id;
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "select * from TestTable1 where ID = ?ID";
                cmd.Parameters.Add("?ID", MySqlDbType.Int32).Value = customer.Id;
                var reader = cmd.ExecuteReader();
                if(reader.Read())
                {
                    customer.Name = reader["NAME"].ToString();
                }                
                conn.Close();
            }
            return customer;
        }

        public List<Customer> GetAllCustomers()
        {
            List<Customer> customers = new List<Customer>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("select * from TestTable1", conn);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        customers.Add(new Customer
                        {
                            Id = Convert.ToInt32(reader["ID"]),
                            Name = reader["NAME"].ToString()
                        });
                    }
                }
                conn.Close();

            }
            return customers;
        }
    }
}
