﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting!");

            StoreContext db = new StoreContext("server=68.79.89.133;port=3306;database=Testing;user=mcwrapper;password=minecraft1234");

            db.AddCustomer(new Customer()
            {
                Id = 0,
                Name = "Console"
            });

            List<Customer> customers = db.GetAllCustomers();
            foreach (var customer in customers)
            {
                Console.WriteLine($"Id: {customer.Id}   Name: {customer.Name}");
            }
        }
    }
}
