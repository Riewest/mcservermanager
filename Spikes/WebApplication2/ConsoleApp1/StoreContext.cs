﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    public class StoreContext
    {
        public string ConnectionString { get; set; }

        public StoreContext(string connectionString)
        {
            ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }

        public void AddCustomer(Customer customer)
        {
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = ("INSERT INTO TestTable1 (NAME) VALUES (?NAME)");
                cmd.Parameters.Add("?NAME", MySqlDbType.VarChar).Value = customer.Name;
                cmd.Transaction = conn.BeginTransaction();
                try
                {
                    cmd.ExecuteNonQuery();
                    cmd.Transaction.Commit();
                }
                catch (Exception e)
                {

                    cmd.Transaction.Rollback();
                }
                conn.Close();
            }
        }

        public List<Customer> GetAllCustomers()
        {
            List<Customer> customers = new List<Customer>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("select * from TestTable1", conn);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        customers.Add(new Customer
                        {
                            Id = Convert.ToInt32(reader["ID"]),
                            Name = reader["NAME"].ToString()
                        });
                    }
                }

            }
                return customers;
        }
    }
}
