﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FileUploadSpike;
using FileUploadSpike.Pages;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace FileUploadSample.Controllers
{
    public class UploadController : Controller
    {
        [HttpPost]
        [DisableRequestSizeLimit]
        [DisableFormValueModelBinding]
        public async Task<IActionResult> Index()
        {
            FormValueProvider formModel;
            using (var stream = System.IO.File.Create(@"C:\Users\chase\Desktop\Minecraft\myfile.temp"))
            {
                formModel = await Request.StreamFile(stream);
            }

            var viewModel = new IndexModel();

            var bindingSuccessful = await TryUpdateModelAsync(viewModel, prefix: "",
               valueProvider: formModel);

            if (!bindingSuccessful)
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
            }

            string filename = Path.GetFileName(viewModel.Filename);
            string testZip = @"C:\Users\chase\Desktop\Minecraft\" + filename;


            System.IO.File.Move(@"C:\Users\chase\Desktop\Minecraft\myfile.temp", testZip);

            return Ok(viewModel);
        }



        //public async Task<IActionResult> Download(string filename)
        //{
        //    if (filename == null)
        //        return Content("filename not present");

        //    var path = Path.Combine(
        //                   Directory.GetCurrentDirectory(),
        //                   "wwwroot", filename);

        //    var memory = new MemoryStream();
        //    using (var stream = new FileStream(path, FileMode.Open))
        //    {
        //        await stream.CopyToAsync(memory);
        //    }
        //    memory.Position = 0;
        //    return File(memory, GetContentType(path), Path.GetFileName(path));
        //}
    }
}